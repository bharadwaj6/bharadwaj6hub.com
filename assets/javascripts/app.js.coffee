# Following are plugin libraries.
#= require ./lib/jquery.min
#= require ./lib/lunr.min
#= require ./lib/handlebars
#= require ./lib/jXHR
#= require ./lib/URI.min
#= require lunr_search

# Following are octopress related (defaults).
#= require ./lib/swfobject-dynamic
#= require modernizr-2.0
#= require octopress

# Instantiate a new search when dom is ready.
$(document).ready ->
    new LunrSearch '#search-query',
                    indexUrl: "/search.json",
                    results: "#search-results",
                    entries: ".entries",
                    template: "#search-results-template"
